package br.mario.message.spring.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableKafka
public class KafkaConfig {

    @Value("${topicos.hello.request.topic}")
    private String helloTopic;

    @Value("${topicos.object.request.topic}")
    private String objectTopic;


    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, String> kafkaFactory() {

        ConcurrentKafkaListenerContainerFactory<String, String> kafkaFactory =
                new ConcurrentKafkaListenerContainerFactory<>();

        kafkaFactory.setConsumerFactory(getCosumerFactory());

        return kafkaFactory;

    }
    private ConsumerFactory<String, String> getCosumerFactory() {

        Map<String, Object> props = new HashMap<>();

        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        return new DefaultKafkaConsumerFactory<>(props);
    }

    @Bean
    public KafkaTemplate<String, String> kafkaTemplate() {

        return new KafkaTemplate<>(getProcuder());

    }
    private ProducerFactory<String, String> getProcuder() {

        Map<String, Object> props = new HashMap<>();

        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);

        return new DefaultKafkaProducerFactory<>(props);

    }

    @Bean
    public NewTopic helloTopicBulider(){
        return TopicBuilder
                .name(helloTopic)
                .partitions(1)
                .replicas(1)
                .build();
    }

    @Bean
    public NewTopic objectTopicBulider(){
        return TopicBuilder
                .name(objectTopic)
                .partitions(1)
                .replicas(1)
                .build();
    }
}

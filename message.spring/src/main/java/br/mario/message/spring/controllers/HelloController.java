package br.mario.message.spring.controllers;

import br.mario.message.spring.services.HelloProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/kafka")
public class HelloController {

    @Autowired
    private HelloProducerService helloProducerService;

    @PostMapping("/{message}")
    public String sendMessage(@PathVariable("message") String message ){

        helloProducerService.sendMessage("Hello "+   message + " !" );
        return "Menssagem Envidando pra Fila";
    }

    @PostMapping()
    public String sendJson(@RequestBody Object obj ){

        helloProducerService.sendObject(obj);
        return "Object Envidando pra Fila";
    }
}

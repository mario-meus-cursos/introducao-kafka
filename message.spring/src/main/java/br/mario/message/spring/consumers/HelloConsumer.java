package br.mario.message.spring.consumers;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class HelloConsumer {

    @KafkaListener(topics = "${topicos.hello.request.topic}", groupId = "group-1")
    public void receiveMesssage(String message){
        System.out.println("Consumer Message: " + message);
    }

    @KafkaListener(topics = "${topicos.object.request.topic}", groupId = "group-1")
    public void receiveObject(String message){
        System.out.println("Consumer Object: " + message);
    }

}

package br.mario.message.spring.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class HelloProducerService {
    @Autowired
    private KafkaTemplate<String,String> stringKafkaTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    @Value("${topicos.hello.request.topic}")
    private String helloTopic;

    @Value("${topicos.object.request.topic}")
    private String objectTopic;

    public void sendMessage(String message){
        this.stringKafkaTemplate.send(helloTopic, message);
    }

    public void sendObject(Object obj) {

        try {

            this.stringKafkaTemplate.send(objectTopic, objectMapper.writeValueAsString(obj));
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Erro ao converter o objecto" + e.getMessage());
        }

    }
}
